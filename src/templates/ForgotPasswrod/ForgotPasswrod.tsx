import React, { useState } from 'react';
import Input from '../../components/Input/Input';
import { useTranslation } from 'react-i18next';
import Button from '../../components/Button/Buttons';
import './ForgotPassword.scss';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import axiosConfig from '../../config/axiosConfig';
import Spinner from '../../components/Spinner/Spinner';

interface FormInput {
    email: string;
}

const schema = yup.object().shape({
    email: yup.string().required().email(),
});

const ForgotPassword = () => {
    const { register, handleSubmit, errors } = useForm<FormInput>({
        resolver: yupResolver(schema),
    });
    const { t } = useTranslation('common');
    const [loading, setLoading] = useState<boolean>(false);
    const [loginSent, setLoginSent] = useState<boolean>(false);
    const [loginError, setLoginError] = useState<number>(0);
    const onSubmit = ({ email }: FormInput): void => {
        setLoading(true);
        axiosConfig
            .post('/auth/forgotPassword', {
                email,
            })
            .then(() => {
                setLoading(false);
                setLoginSent(true);
            })
            .catch((e) => {
                setLoginError(e);
                setLoginSent(true);
                console.log(e);
            });
    };

    if (!loginSent) {
        return (
            <div className='forgotPassword-container'>
                {loading ? (
                    <Spinner />
                ) : (
                    <form className='forgotPassword-form' onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            register={register}
                            id='email'
                            label={t('registrationPage.email')}
                            type='email'
                            placeholder={t('hints.email')}
                        />
                        {errors?.email && <p className='error-msg'>{t('errorMessage.emailField')}</p>}
                        <Button label={t('forgotPasswordPage.sendNewPassword')} click={handleSubmit(onSubmit)} />
                    </form>
                )}
            </div>
        );
    } else {
        return (
            <div>
                {loginError ? (
                    <h3>{t('forgotPasswordPage.failMessage')}</h3>
                ) : (
                    <h3>{t('forgotPasswordPage.successMessage')}</h3>
                )}
            </div>
        );
    }
};

export default ForgotPassword;
