import React from 'react';
import { NavLink } from 'react-router-dom';
import { routes } from '../../routes/routes';
import TransparentButton from '../../components/TransparentButton/TransparentButton';
import { useTranslation } from 'react-i18next';
import './RegistrationError.scss';

type RegistrationErrorProps = {
    readonly errorCode: number;
};

const RegistrationError = ({ errorCode }: RegistrationErrorProps) => {
    const { t } = useTranslation('common');

    if (errorCode === 500) {
        return (
            <div className='error-container'>
                <p>{t('errorMessage.serverNotWorking')}</p>
            </div>
        );
    } else {
        return (
            <div className='error-container'>
                <p>{t('registrationPage.failedMessage')}</p>
                <NavLink to={routes.registration} className='link'>
                    <TransparentButton label={t('registrationPage.createAccount')} />
                </NavLink>
                <p> lub </p>
                <NavLink to={routes.forgotPassword} className='link'>
                    <TransparentButton label={t('registrationPage.forgotPassword')} />
                </NavLink>
            </div>
        );
    }
};

export default RegistrationError;
