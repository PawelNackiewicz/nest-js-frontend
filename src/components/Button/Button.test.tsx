import { render } from '@testing-library/react';
import React from 'react';
import Button from './Buttons';

describe('Button component', () => {
    it('renders default element', () => {
        const { getByTestId } = render(<Button click={() => {}} label='test-label' />);
        const renderedButton = getByTestId('button');
        expect(renderedButton).toBeInTheDocument();
        expect(renderedButton.innerHTML).toContain('test-label');
        expect(renderedButton).toHaveClass('button');
    });
});
