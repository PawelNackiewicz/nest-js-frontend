import React from 'react';
import { render } from '@testing-library/react';
import TransparentButton from './TransparentButton';

describe('Transparent button component', () => {
    it('renders default element', () => {
        const { getByTestId } = render(<TransparentButton label='test-label' />);
        const renderedButton = getByTestId('transparent-button');
        expect(renderedButton).toBeInTheDocument();
        expect(renderedButton.innerHTML).toContain('test-label');
        expect(renderedButton).toHaveClass('button--transparent');
    });
});
