import React from 'react';
import './TransparentButton.scss';

type ButtonProps = {
    readonly label: string;
};

const TransparentButton = ({ label }: ButtonProps) => {
    return (
        <button className='button--transparent' data-testid='transparent-button'>
            {label}
        </button>
    );
};

export default TransparentButton;
