export const routes = {
    aboutProject: '/o-projekcie',
    confirmUser: '/auth/confirm',
    forgotPassword: '/przypomnij-haslo',
    home: '/',
    login: '/logowanie',
    registration: '/rejestracja',
    resetPassword: '/auth/resetPassword',
    rules: '/regulamin',
    myObject: '/moje-obiekty',
    settings: '/ustawienia',
};
