import React from 'react';
import './Footer.scss';

const Footer = () => {
    return (
        <div className='footer-container' data-testid='footer'>
            <p>Copyrights 2020</p>
        </div>
    );
};

export default Footer;
