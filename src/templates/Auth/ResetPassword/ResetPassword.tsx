import React, { useEffect, useState } from 'react';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import axiosConfig from '../../../config/axiosConfig';
import Spinner from '../../../components/Spinner/Spinner';
import Input from '../../../components/Input/Input';
import Button from '../../../components/Button/Buttons';
import { NavLink, RouteComponentProps, useLocation } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import './ResetPassword.scss';
import { routes } from '../../../routes/routes';
import TransparentButton from '../../../components/TransparentButton/TransparentButton';

interface FormInput {
    password: string;
    confirmPassword: string;
}

const schema = yup.object().shape({
    password: yup.string().min(8),
    confirmPassword: yup.string().oneOf([yup.ref('password')]),
});

const ResetPassword = (props: RouteComponentProps) => {
    useEffect(() => {
        setLoading(false);
        setChangePasswordRequestSent(false);
        setChangePasswordError(0);
    }, [props]);

    const [loading, setLoading] = useState<boolean>(false);
    const [changePasswordRequestSent, setChangePasswordRequestSent] = useState<boolean>(false);
    const [changePasswordError, setChangePasswordError] = useState<number>(0);
    const { register, handleSubmit, errors } = useForm<FormInput>({
        resolver: yupResolver(schema),
    });
    const { t } = useTranslation('common');
    const token = useLocation().search.slice(1);

    const onSubmit = (data: FormInput): void => {
        setLoading(true);
        const { password } = data;
        axiosConfig
            .patch('/auth/changePassword', {
                password,
                token,
            })
            .then(() => {
                setLoading(false);
                setChangePasswordRequestSent(true);
            })
            .catch((e) => {
                setChangePasswordRequestSent(true);
                setChangePasswordError(e.response.status);
            });
    };

    if (!changePasswordRequestSent) {
        return (
            <div>
                {loading ? (
                    <div>
                        <Spinner />
                    </div>
                ) : (
                    <form onSubmit={handleSubmit(onSubmit)} className='resetPassword-container'>
                        <Input
                            register={register}
                            id='password'
                            label={t('registrationPage.password')}
                            type='password'
                            placeholder={t('hints.password')}
                        />
                        {errors?.password?.type === 'min' && (
                            <p className='error-msg'>{t('errorMessage.password.min8Chars')}</p>
                        )}
                        <Input
                            register={register}
                            id='confirmPassword'
                            label={t('hints.confirmPassword')}
                            type='password'
                            placeholder={t('registrationPage.confirmPassword')}
                        />
                        {errors?.confirmPassword?.type === 'oneOf' && (
                            <p className='error-msg'>{t('errorMessage.password.equal')}</p>
                        )}
                        <Button label={t('resetPassword.resetButton')} click={handleSubmit(onSubmit)} />
                    </form>
                )}
            </div>
        );
    } else {
        return (
            <div>
                {changePasswordError ? (
                    <p>{t('resetPassword.errorMessage')}</p>
                ) : (
                    <div className='resetPassword-container'>
                        <p>{t('resetPassword.successMessage')}</p>
                        <NavLink to={routes.login} className='link'>
                            <TransparentButton label={t('resetPassword.goToLogin')} />
                        </NavLink>
                    </div>
                )}
            </div>
        );
    }
};

export default ResetPassword;
