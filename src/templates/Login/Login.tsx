import React, { useState } from 'react';
import './Login.scss';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Buttons';
import { useTranslation } from 'react-i18next';
import TransparentButton from '../../components/TransparentButton/TransparentButton';
import { NavLink, Redirect } from 'react-router-dom';
import { routes } from '../../routes/routes';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import axiosConfig from '../../config/axiosConfig';
import Spinner from '../../components/Spinner/Spinner';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../../store/session/sessionSlice';
import { RootState } from '../../store';
import { User } from '../../store/session/types';

interface FormInput {
    email: string;
    password: string;
}

const schema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().min(8),
});

const Login = () => {
    const dispatch = useDispatch();
    const user = useSelector<RootState, User>((state) => state.session.user as User);
    const { register, handleSubmit, errors } = useForm<FormInput>({
        resolver: yupResolver(schema),
    });
    const [loading, setLoading] = useState<boolean>(false);
    const { t } = useTranslation('common');
    const getUser = (): void => {
        axiosConfig.get('/auth/session/me').then((response) => {
            toast.success(response.data.email);
            const { email, firstName, lastName, _id } = response.data;
            dispatch(
                setUser({
                    email,
                    firstName,
                    lastName,
                    id: _id,
                }),
            );
        });
    };

    const onSubmit = (data: FormInput): void => {
        setLoading(true);
        const { email, password } = data;
        axiosConfig
            .post('/auth/login', {
                email,
                password,
            })
            .then(() => {
                setLoading(false);
                getUser();
            })
            .catch((e) => {
                setLoading(false);
                toast.error(t('loginPage.failedLogin'));
                console.log(e);
            });
    };

    if (user) {
        return <Redirect to={routes.home} />;
    }

    return (
        <div>
            {loading ? (
                <div className='spinner-container'>
                    <Spinner />
                </div>
            ) : (
                <div className='login-container'>
                    <form className='login-form' onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            register={register}
                            id='email'
                            required
                            label={t('registrationPage.email')}
                            type='email'
                            placeholder={t('hints.email')}
                        />
                        {errors?.email && <p className='error-msg'>{t('errorMessage.emailField')}</p>}
                        <Input
                            register={register}
                            id='password'
                            required
                            label={t('registrationPage.password')}
                            type='password'
                            placeholder={t('hints.password')}
                        />
                        {errors?.password?.type === 'min' && (
                            <p className='error-msg'>{t('errorMessage.password.min8Chars')}</p>
                        )}
                        <Button label={t('loginPage.loginButton')} click={handleSubmit(onSubmit)} />
                    </form>
                    <div className='hints-container'>
                        <div className='hint-item'>
                            <p>{t('loginPage.hasAccount')}</p>
                            <NavLink to={routes.registration} className='link'>
                                <TransparentButton label={t('loginPage.createAccount')} />
                            </NavLink>
                        </div>
                        <NavLink to={routes.forgotPassword} className='link'>
                            <TransparentButton label={t('loginPage.forgotPassword')} />
                        </NavLink>
                    </div>
                </div>
            )}
        </div>
    );
};

export default Login;
