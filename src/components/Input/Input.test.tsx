import { fireEvent, render } from '@testing-library/react';
import Input from './Input';
import React from 'react';

describe('Input component', () => {
    it('renders default element', () => {
        const { getByTestId } = render(<Input />);
        expect(getByTestId('input')).toBeInTheDocument();
    });

    it('renders input element with placeholder', () => {
        const placeholderText = 'test placeholder';
        const { getByPlaceholderText } = render(<Input placeholder={placeholderText} />);
        expect(getByPlaceholderText(placeholderText)).toBeInTheDocument();
    });

    it('renders input element with custom type', () => {
        const { getByTestId } = render(<Input type='password' />);
        expect(getByTestId('input')).toHaveAttribute('type', 'password');
    });

    it('display proper input value', () => {
        const { getByTestId } = render(<Input />);
        fireEvent.change(getByTestId('input'), {
            target: { value: 'input value' },
        });
        expect(getByTestId('input')).toHaveValue('input value');
    });
});
