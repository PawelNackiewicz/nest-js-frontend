import { configureStore } from '@reduxjs/toolkit';
import sessionSlice from './session/sessionSlice';
import { useDispatch } from 'react-redux';

export const store = configureStore({
    reducer: {
        session: sessionSlice,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
