import React from 'react';
import Loader from 'react-loader-spinner';

const Spinner = () => {
    return <Loader type='Rings' color='#83A603' height={100} width={100} />;
};
export default Spinner;
