import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import pl from './assets/translations/pl.json';
import 'react-toastify/dist/ReactToastify.css';

i18next
    .init({
        fallbackLng: 'pl',
        interpolation: { escapeValue: false },
        lng: navigator.language,
        resources: {
            pl: {
                common: pl,
            },
        },
    })
    .then();

ReactDOM.render(
    <React.StrictMode>
        <I18nextProvider i18n={i18next}>
            <App />
        </I18nextProvider>
    </React.StrictMode>,
    document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
