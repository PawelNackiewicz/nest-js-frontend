import React from 'react';
import LeafletMap from '../../components/LeafletMap/LeafletMap';

const Map = () => {
    return <LeafletMap />;
};

export default Map;
