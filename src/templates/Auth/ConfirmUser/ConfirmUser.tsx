import React, { useCallback, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import axiosConfig from '../../../config/axiosConfig';
import Loader from 'react-loader-spinner';
import { useTranslation } from 'react-i18next';
import './ConfirmUser.scss';

const ConfirmUser = () => {
    const token = useLocation().search;
    const [confirmation, setConfirmation] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const { t } = useTranslation('common');

    const confirmUser = useCallback(() => {
        axiosConfig
            .get(`/auth/confirm/${token}`, {})
            .then(() => {
                setConfirmation(true);
            })
            .catch(() => {
                setConfirmation(false);
            })
            .finally(() => {
                setIsLoading(false);
            });
    }, [token]);

    useEffect(() => {
        confirmUser();
    }, [confirmUser]);

    return (
        <div className='confirmUser-container'>
            {isLoading ? (
                <Loader />
            ) : (
                <div>
                    {confirmation ? <p>{t('confirmUser.successMessage')}</p> : <p>{t('confirmUser.failMessage')}</p>}
                </div>
            )}
        </div>
    );
};

export default ConfirmUser;
