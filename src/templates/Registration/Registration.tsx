import React, { useEffect, useState } from 'react';
import Input from '../../components/Input/Input';
import './Registration.scss';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Button from '../../components/Button/Buttons';
import Spinner from '../../components/Spinner/Spinner';
import { RouteComponentProps } from 'react-router-dom';
import axiosConfig from '../../config/axiosConfig';
import RegistrationError from './RegistrationError';

interface FormInput {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
}

const schema = yup.object().shape({
    email: yup.string().email().required(),
    firstName: yup.string().required(),
    lastName: yup.string().required(),
    password: yup.string().min(8),
    confirmPassword: yup.string().oneOf([yup.ref('password')]),
});

const Registration = (props: RouteComponentProps) => {
    useEffect(() => {
        setLoading(false);
        setRegistrationSent(false);
        setRegistrationError(0);
    }, [props]);

    const [loading, setLoading] = useState<boolean>(false);
    const [registrationSent, setRegistrationSent] = useState<boolean>(false);
    const [registrationError, setRegistrationError] = useState<number>(0);
    const { register, handleSubmit, errors } = useForm<FormInput>({
        resolver: yupResolver(schema),
    });
    const { t } = useTranslation('common');
    const onSubmit = (data: FormInput): void => {
        setLoading(true);
        const { firstName, lastName, email, password } = data;
        axiosConfig
            .post('/auth/register', {
                firstName,
                lastName,
                email,
                password,
            })
            .then(() => {
                setLoading(false);
                setRegistrationSent(true);
            })
            .catch((e) => {
                setRegistrationSent(true);
                if (e.response) {
                    setRegistrationError(e.response.status);
                } else {
                    setRegistrationError(500);
                }
            });
    };

    if (!registrationSent) {
        return (
            <div className='registration-container'>
                {loading ? (
                    <div className='spinner-container'>
                        <Spinner />
                    </div>
                ) : (
                    <form className='registration-form' onSubmit={handleSubmit(onSubmit)}>
                        <Input
                            register={register}
                            id='email'
                            label={t('registrationPage.email')}
                            type='email'
                            placeholder={t('hints.email')}
                        />
                        {errors?.email && <p className='error-msg'>{t('errorMessage.emailField')}</p>}
                        <Input
                            register={register}
                            id='firstName'
                            label={t('registrationPage.firstName')}
                            placeholder={t('hints.firstName')}
                        />
                        {errors?.firstName?.type === 'required' && (
                            <p className='error-msg'>{t('errorMessage.fieldRequired')}</p>
                        )}
                        <Input
                            register={register}
                            id='lastName'
                            label={t('registrationPage.lastName')}
                            placeholder={t('hints.lastName')}
                        />
                        {errors?.lastName?.type === 'required' && (
                            <p className='error-msg'>{t('errorMessage.fieldRequired')}</p>
                        )}
                        <Input
                            register={register}
                            id='password'
                            label={t('registrationPage.password')}
                            type='password'
                            placeholder={t('hints.password')}
                        />
                        {errors?.password?.type === 'min' && (
                            <p className='error-msg'>{t('errorMessage.password.min8Chars')}</p>
                        )}
                        <Input
                            register={register}
                            id='confirmPassword'
                            label={t('registrationPage.confirmPassword')}
                            type='password'
                            placeholder={t('hints.confirmPassword')}
                        />
                        {errors?.confirmPassword?.type === 'oneOf' && (
                            <p className='error-msg'>{t('errorMessage.password.equal')}</p>
                        )}
                        <div className='regulations-container'>
                            <p>{t('regulations.regulationMessage')}</p>
                        </div>
                        <Button label={t('registrationPage.registrationButton')} click={handleSubmit(onSubmit)} />
                    </form>
                )}
            </div>
        );
    } else {
        return (
            <div className='information-container'>
                {registrationError ? (
                    <RegistrationError errorCode={registrationError} />
                ) : (
                    <p>{t('registrationPage.successMessage')}</p>
                )}
            </div>
        );
    }
};

export default Registration;
