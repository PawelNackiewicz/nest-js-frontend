import React from 'react';
import './Button.scss';

type ButtonProps = {
    readonly label: string;
    readonly click: () => void;
};

const Button = ({ label, click }: ButtonProps) => {
    return (
        <button onClick={click} className='button' data-testid='button'>
            {label}
        </button>
    );
};

export default Button;
