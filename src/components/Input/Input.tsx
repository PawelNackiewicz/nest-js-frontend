import React from 'react';
import './Input.scss';

type RefReturn =
    | string
    | ((instance: HTMLInputElement | null) => void)
    | React.RefObject<HTMLInputElement>
    | null
    | undefined;

type InputProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    readonly placeholder?: string;
    readonly type?: string;
    readonly label?: string;
    readonly id: string;
    readonly register: ({ required }: { required?: boolean }) => RefReturn;
};

const Input = ({ id, register, required, placeholder, type, label }: InputProps) => {
    return (
        <div className='input-container'>
            <label htmlFor={id}>{label}</label>
            <input
                name={id}
                ref={register({ required })}
                placeholder={placeholder}
                type={type}
                className='input'
                data-testid='input'
            />
        </div>
    );
};
export default Input;
