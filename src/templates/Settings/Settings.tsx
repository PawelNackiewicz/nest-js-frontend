import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { User } from '../../store/session/types';
import { Redirect } from 'react-router-dom';
import { routes } from '../../routes/routes';

export const Settings = () => {
    const user = useSelector<RootState, User>((state) => state.session.user as User);
    console.log(user);
    if (user) {
        return (
            <div>
                <h1>Hello Settings</h1>
            </div>
        );
    }

    return <Redirect to={routes.login} />;
};
