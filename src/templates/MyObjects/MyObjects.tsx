import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { User } from '../../store/session/types';
import { Redirect } from 'react-router-dom';
import { routes } from '../../routes/routes';
import { useTranslation } from 'react-i18next';
import { FacilityItem } from '../../components/FacilityItem/FacilityItem';
import './MyOcjects.scss';

type Facility = {
    id: string;
    name: string;
    address: string;
};

const facilities: Facility[] = [
    {
        id: '1',
        name: 'miejsce 1',
        address: 'Kopernika 14',
    },
    {
        id: '2',
        name: 'miejsce 2',
        address: 'Fajna 12',
    },
    {
        id: '3',
        name: 'super 3',
        address: 'super 22',
    },
];

export const MyObject = () => {
    const user = useSelector<RootState, User>((state) => state.session.user as User);
    const { t } = useTranslation('common');

    if (user) {
        return (
            <div className='facility-container'>
                <div className='facilityList'>
                    <ul>
                        {facilities.map((facility) => {
                            return <FacilityItem facility={facility} key={facility.id} />;
                        })}
                    </ul>
                </div>
                <button className='button'>{t('myObjectPage.addNewObject')}</button>
            </div>
        );
    }

    return <Redirect to={routes.login} />;
};
