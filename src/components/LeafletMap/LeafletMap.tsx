import React from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';
import { LatLngTuple } from 'leaflet';
import './LeafletMap.scss';

type MarkerType = {
    readonly key: string;
    readonly position: [number, number];
    readonly content: string;
};

const defaultLatLng: LatLngTuple = [50.675106, 17.921297];
const zoom = 12;

const MyMarkersList = ({ markers }: { markers: Array<MarkerType> }) => {
    const items = markers.map(({ key, ...props }) => <MyPopupMarker key={key} {...props} />);
    return <>{items}</>;
};

type Position = [number, number];

type MarkerProps = {
    readonly content: string;
    readonly position: Position;
};

const MyPopupMarker = ({ content, position }: MarkerProps) => (
    <Marker position={position}>
        <Popup>{content}</Popup>
    </Marker>
);

const LeafletMap = () => {
    const markers: MarkerType[] = [
        {
            key: 'marker1',
            position: [50.675106, 17.921297],
            content: 'My first popup',
        },
        {
            key: 'marker2',
            position: [50.670789, 17.988637],
            content: 'My second popup',
        },
        {
            key: 'marker3',
            position: [50.652726, 17.941259],
            content: 'My third popup',
        },
    ];

    return (
        <MapContainer id='mapId' center={defaultLatLng} zoom={zoom}>
            <TileLayer
                url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            />
            <MyMarkersList markers={markers} />
        </MapContainer>
    );
};

export default LeafletMap;
