import React from 'react';
import { BrowserRouter as Router, Route, RouteComponentProps } from 'react-router-dom';
import Header from './components/Header/Header.';
import { routes } from './routes/routes';
import Login from './templates/Login/Login';
import Registration from './templates/Registration/Registration';
import AboutProject from './templates/AboutProject/AboutProject';
import './App.scss';
import Footer from './components/Footer/Footer';
import ForgotPassword from './templates/ForgotPasswrod/ForgotPasswrod';
import Rules from './templates/Rules/Rules';
import Map from './templates/Map/Map';
import ConfirmUser from './templates/Auth/ConfirmUser/ConfirmUser';
import ResetPassword from './templates/Auth/ResetPassword/ResetPassword';
import { ToastContainer } from 'react-toastify';
import { Provider } from 'react-redux';
import { store } from './store';
import { MyObject } from './templates/MyObjects/MyObjects';
import { Settings } from './templates/Settings/Settings';

const App = () => {
    return (
        <Provider store={store}>
            <Router>
                <Header />
                <div className='content-container'>
                    <Route path={routes.home} exact component={Map} />
                    <Route path={routes.aboutProject} exact component={AboutProject} />
                    <Route
                        path={routes.registration}
                        exact
                        component={(props: RouteComponentProps) => <Registration {...props} />}
                    />
                    <Route path={routes.login} exact component={Login} />
                    <Route path={routes.myObject} exact component={MyObject} />
                    <Route path={routes.settings} exact component={Settings} />
                    <Route path={routes.forgotPassword} exact component={ForgotPassword} />
                    <Route path={routes.rules} exact component={Rules} />
                    <Route path={routes.confirmUser} exact component={ConfirmUser} />
                    <Route path={routes.resetPassword} exact component={ResetPassword} />
                </div>
                <ToastContainer position='bottom-center' autoClose={5000} />
                <Footer />
            </Router>
        </Provider>
    );
};

export default App;
