import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User } from './types';

type stateType = {
    user: User | null;
};

const initialState: stateType = {
    user: null,
};

const sessionSlice = createSlice({
    name: 'session',
    initialState,
    reducers: {
        setUser(state, action: PayloadAction<User>) {
            state.user = action.payload;
        },
        removeUser(state) {
            state.user = null;
        },
    },
});

export const { setUser, removeUser } = sessionSlice.actions;
export default sessionSlice.reducer;
